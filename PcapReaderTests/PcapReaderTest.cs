﻿using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PcapReader;
using System.Collections.Generic;

namespace PcapReaderTests
{
    [TestClass]
    public class PcapReaderTest
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PcapReaderTest));

        [TestMethod]
        public void when_file_path_is_null()
        {
            log.Info("read_pcap_file_when_file_is_empty");
            List<Packet> actual = PcapFile.Read(null);
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void when_file_path_is_empty()
        {
            log.Info("read_pcap_file_when_file_is_empty");
            List<Packet> actual = PcapFile.Read(string.Empty);
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void when_file_is_valid()
        {
            log.Info("read_pcap_file_when_file_is_valid");
            string filePath = @"..\..\SampleFiles\udp_lite_full_coverage_0.pcap";
            List<Packet> actual = PcapFile.Read(filePath);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void when_packet_number_is_1()
        {
            log.Info("read_pcap_file_when_packet_number_is_1");
            string filePath = @"..\..\SampleFiles\udp_lite_full_coverage_0.pcap";
            List<Packet> packet = PcapFile.Read(filePath);
            int actual = packet.Count;
            Assert.AreEqual(1, actual);
        }

        [TestMethod]
        public void to_check_if_src_mac_is_correct()
        {
            log.Info("read_pcap_file_to_check_if_src_mac_is_correct");
            string filePath = @"..\..\SampleFiles\udp_lite_full_coverage_0.pcap";
            List<Packet> packets = PcapFile.Read(filePath);
            Packet packet = packets[0];
            byte[] actual = packet.SrcMac;
            byte[] expected = new byte[] { 0x00, 0x04, 0x75, 0xc7, 0x87, 0x49 };
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void to_check_if_dst_mac_is_correct()
        {
            log.Info("read_pcap_file_to_check_if_dst_mac_is_correct");
            string filePath = @"..\..\SampleFiles\udp_lite_full_coverage_0.pcap";
            List<Packet> packets = PcapFile.Read(filePath);
            Packet packet = packets[0];
            byte[] actual = packet.DstMac;
            byte[] expected = new byte[] { 0x00, 0x04, 0x76, 0xdd, 0xbb, 0x3a };
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void to_check_if_data_is_correct()
        {
            log.Info("read_pcap_file_to_check_if_data_is_correct");
            string filePath = @"..\..\SampleFiles\udp_lite_full_coverage_0.pcap";
            List<Packet> packets = PcapFile.Read(filePath);
            Packet packet = packets[0];
            byte[] actual = packet.Data;
            byte[] expected = new byte[] {
                0x08, 0x00, 0x45, 0x00, 0x00, 0x28, 0x1a, 0x6a, 0x40, 0x00,
                0x40, 0x88, 0x6f, 0x71, 0x8b, 0x85, 0xcc, 0xb0, 0x8b, 0x85,
                0xcc, 0xb7, 0x80, 0x00, 0x04, 0xd2, 0x00, 0x00, 0x38, 0x45,
                0x68, 0x65, 0x6c, 0x6c, 0x6f, 0x20, 0x77, 0x6f, 0x72, 0x6c,
                0x64, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void when_packet_number_is_13()
        {
            log.Info("read_pcap_file_when_packet_number_is_13");
            string filePath = @"..\..\SampleFiles\udp_lite_normal_coverage_8-20.pcap";
            List<Packet> packet = PcapFile.Read(filePath);
            int actual = packet.Count;
            Assert.AreEqual(13, actual);
        }

        [TestMethod]
        public void when_packet_number_is_4675()
        {
            log.Info("when_packet_number_is_4675");
            string filePath = @"..\..\SampleFiles\redundant_stream1.pcap";
            List<Packet> packet = PcapFile.Read(filePath);
            int actual = packet.Count;
            Assert.AreEqual(4675, actual);
        }
    }
}
