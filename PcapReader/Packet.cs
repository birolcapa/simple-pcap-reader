﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PcapReader
{
    /// <summary>
    /// An ethernet packet
    /// Maximum length of an ethernet packet
    /// https://wiki.wireshark.org/MTU
    /// The MTU is the maximum payload length for a particular transmission media. 
    /// For example, the MTU for Ethernet is typically 1500 bytes.
    /// (The maximum packet length for Ethernet is typically 1518 bytes, 
    /// but that includes 14 bytes of Ethernet header
    /// and 4 bytes of CRC, 
    /// leaving 1500 bytes of payload.) 
    /// Packet format
    /// A physical Ethernet packet will look like this:
    /// Preamble                    8 - Removed by hardware, Wireshark does not get thıs.
    /// Destination MAC address     6
    /// Source MAC address          6
    /// Type/Length                 2
    /// User Data					46-1500
    /// Frame Check Sequence (FCS)  4 - Removed by hardware, Wireshark does not get this
    /// Form Wireshark's point of view: 6 + 6 + 2 + 1500 = 1514 byte (max packet length)
    /// So user data can be thought as 1514 - 6 -  6 = 1502
    /// If a host wishes to send packet larger than the MTU for a network, 
    /// the packet must be broken up into chunks no larger than the MTU.
    /// </summary> 
    public class Packet
    {
        /// <summary>
        /// Size of a mac address
        /// </summary>
        public const ushort s_MacSize = 6;

        /// <summary>
        /// Calculated according to 
        /// Type/Length (2) + User Data (min: 46, max: 1500)
        /// 2 + 1500 = 1502
        /// </summary>
        public const ushort s_MaxPacketDataSize = 1502;

        //Frame TimeStamp - Holds Second Part
        public uint TimestampSec { get; internal set; }

        //Frame TimeStamp - Holds Nanosecond Part
        public uint TimestampNsec { get; internal set; }

        //Packet Length
        public uint Length { get; internal set; }

        /// <summary>
        /// Source Mac Address
        /// </summary>
        public byte[] SrcMac { get; internal set; } = new byte[s_MacSize];

        /// <summary>
        /// Destination Mac Address
        /// </summary>
        public byte[] DstMac { get; internal set; } = new byte[s_MacSize];

        /// <summary>
        /// User Data
        /// </summary>
        public byte[] Data { get; internal set; } = new byte[s_MaxPacketDataSize];

        /// <summary>
        /// Packet Number
        /// </summary>
        public uint Number { get; internal set; }
    }
}
