﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PcapReader
{
    public class PcapFile
    {
        // Create a logger for use in this class
        private static readonly log4net.ILog log = 
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Pcap file has a 24 Byte header
        /// </summary>
        private const ushort s_PcapFileHeaderLength = 24;

        /// <summary>
        /// Timestamp = 8 byte
        /// </summary>
        private const ushort s_TimeStamp = 8;

        /// <summary>
        /// Capture length = 4 byte
        /// </summary>
        private const ushort s_CaptureLength = 4;

        /// <summary>
        /// Packet length = 4 byte
        /// </summary>
        private const ushort s_PacketLength = 4;

        public static List<Packet> Read(string inputFilePath)
        {
            bool isPathValid = CheckInputFilePath(inputFilePath);

            if (!isPathValid)
            {
                return null;
            }

            //Filestream and binaryreader definitions to open pcap file
            FileStream fileStream = new FileStream(inputFilePath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);

            //Read Packet header of the pcap file
            uint fileSize = ReadPacketHeader(binaryReader);

            List<Packet> packets = new List<Packet>();
            ReadPackets(fileSize, binaryReader, packets);
            return packets;
        }

        private static bool CheckInputFilePath(string inputFilePath)
        {
            if (!File.Exists(inputFilePath))
            {
                // Log an error with an exception
                log.Error("File cannot be found: " + inputFilePath);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Read packet header
        /// </summary>
        /// <param name="binaryReader">Binary reader</param>
        /// <returns>Size of pcap file</returns>
        private static uint ReadPacketHeader(BinaryReader binaryReader)
        {
            uint fileSize = 0;
            try
            {
                // https://wiki.wireshark.org/Development/LibpcapFileFormat
                byte[] oArrayHeader = new byte[]
                {
                    0x00, 0x00, 0x00, 0x00, // magic number = 0xa1b23c4d
                    0x02, 0x00, // major version number
                    0x04, 0x00, // minor version number
                    0x00, 0x00, 0x00, 0x00, // GMT to local correction
                    0x00, 0x00, 0x00, 0x00, // accuracy of timestamps
                    0xFF, 0xFF, 0x00, 0x00, // max. length of captured packets
                    0x01, 0x00, 0x00, 0x00 // data link type
                };
                binaryReader.Read(oArrayHeader, 0, oArrayHeader.Length);
                fileSize = (uint)binaryReader.BaseStream.Length - s_PcapFileHeaderLength;
            }
            catch (Exception ex)
            {
                log.Error("An error occured while reading packet header: " + ex.Message);
            }
            return fileSize;
        }

        private static void ReadPackets(uint fileSize, BinaryReader binaryReader, List<Packet> packets)
        {
            uint packetNumber = 0;

            while (fileSize > 0)
            {
                packetNumber++;
                Packet packet = new Packet();
                ReadPacketData(binaryReader, ref packet, ref fileSize);
                packet.Number = packetNumber;
                packets.Add(packet);
            }
            log.Info("Progressed Packets: " + packetNumber);
        }

        /// <summary>
        /// Read packet data
        /// </summary>
        /// <param name="binaryReader">Binary reader</param>
        /// <param name="packet">Packet</param>
        /// <param name="fileSize">Size of pcap file</param>
        private static void ReadPacketData(BinaryReader binaryReader, ref Packet packet, ref uint fileSize)
        {
            try
            {
                // See Record (Packet) Header at
                // https://wiki.wireshark.org/Development/LibpcapFileFormat

                // Get Timestamp
                packet.TimestampSec = binaryReader.ReadUInt32();
                packet.TimestampNsec = binaryReader.ReadUInt32();

                // Get length
                packet.Length = binaryReader.ReadUInt32();
                packet.Length = binaryReader.ReadUInt32();

                packet.DstMac = binaryReader.ReadBytes((int)Packet.s_MacSize);
                packet.SrcMac = binaryReader.ReadBytes((int)Packet.s_MacSize);
                packet.Data = binaryReader.ReadBytes((int)packet.Length - Packet.s_MacSize - Packet.s_MacSize);

                // update fileSize
                //	- Timestamp		= 8 byte
                //	- CaptureLength	= 4 byte
                //	- PacketLength	= 4 byte
                fileSize -= (packet.Length + s_TimeStamp + s_CaptureLength + s_PacketLength);
            }
            catch (Exception ex)
            {
                log.Error("An error occured while reading packet data: " + ex.Message);
            }
        }
    }
}
