using log4net;
using PcapReader;
using System;
using System.Collections.Generic;

namespace PcapReaderApp
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            log.Info("Sample Pcap Reader application is started.");
            string pcapFilePath = @"..\..\..\PcapReaderTests\SampleFiles\udp_lite_full_coverage_0.pcap";
            List<Packet> packets = PcapFile.Read(pcapFilePath);
            foreach (Packet packet in packets)
            {
                Console.WriteLine("Packet number " + packet.Number + 
                    "'s Properties: TimestampSec: " + packet.TimestampSec + 
                    " TimestampNsec: " + packet.TimestampNsec +
                    " Dst Mac: " + BitConverter.ToString(packet.DstMac) +
                    " Src Mac: " + BitConverter.ToString(packet.SrcMac));
            }

            Console.ReadKey();
        }
    }
}
